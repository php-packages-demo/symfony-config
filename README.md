# symfony-config

Find, load, combine, fill and validate configuration values. [symfony/config](https://packagist.org/packages/symfony/config)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* [*Symfony Internals #1: Inside the Framework Configuration*
  ](https://medium.com/the-sensiolabs-tech-blog/symfony-internals-1-inside-the-framework-configuration-8235ec3fecdf)
  2022-03 Alexandre Daubois
* [*10 Cool Features You Get after switching from YAML to PHP Configs*
  ](https://tomasvotruba.com/blog/2020/07/16/10-cool-features-you-get-after-switching-from-yaml-to-php-configs/)
  2020-07 tomasvotruba
